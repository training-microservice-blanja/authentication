# Aplikasi Auth Server #

Flow Grant Type : 

* Authorization Code
* Implicit
* Resource Owner Password
* Client Credentials

## Authorization Code ##

1. Mendapatkan `authorization code`. Browse ke [http://localhost:8080/oauth/authorize?client_id=client001&grant_type=authorization_code&response_type=code](http://localhost:8080/oauth/authorize?client_id=client001&grant_type=authorization_code&response_type=code)

2. Setelah login, user akan diredirect sesuai dengan `redirect_uri` yang didaftarkan untuk `client001`. Misalnya hasilnya seperti ini:

        http://localhost:10000/handle-oauth-callback?code=zruOLr
    
    Authorization code : `zruOLr`

3. Tukarkan authorization code menjadi `access_token`. Caranya : request `POST` ke [http://localhost:8080/oauth/token](http://localhost:8080/oauth/token) dengan spesifikasi :

    * Basic Authentication : client id dan client secret
    
    * Request Parameter :
    
        * client_id
        * grant_type
        * code
    
    Dengan Postman, seperti ini
    
    [![Postman Auth Code](docs/img/postman-auth-code.png)](docs/img/postman-auth-code.png)
    
    Hasilnya seperti ini
    
        ```json
	    {
	        "access_token": "7521bfed-7aac-4773-a2af-46eab7f67b61",
	        "token_type": "bearer",
	        "refresh_token": "f4d02c4a-0af9-4aaf-9454-bd17e0487ac5",
	        "expires_in": 43199,
	        "scope": "entri_data"
	    }
	    ```


4. Periksa apakah `access_token` valid. Lakukan `POST` ke [http://localhost:8080/oauth/check_token](http://localhost:8080/oauth/check_token) dengan spesifikasi:

    * Basic Authentication
    
    * Request Parameter:
    
        * `token` : `7521bfed-7aac-4773-a2af-46eab7f67b61`
        
    Dengan Postman seperti ini
    
    [![Postman Check Token](docs/img/postman-check-token.png)](docs/img/postman-check-token.png)
    
    Hasilnya seperti ini
    
        ```json
	    {
	        "aud": [
	            "belajar"
	        ],
	        "user_name": "user001",
	        "scope": [
	            "review_transaksi",
	            "approve_transaksi"
	        ],
	        "active": true,
	        "exp": 1544209904,
	        "authorities": [
	            "VIEW_TRANSAKSI"
	        ],
	        "client_id": "client001"
	    }
	    ```

## Ganti format token menjadi JWT ##

1. Buat key pair (pasangan public dan private key, disimpan dalam keystore)

        keytool -genkey -alias authserver -keystore src
        /main/resources/authserver.pfx -storetype pkcs12 -keyalg RSA
    
    Kita akan disuruh menjawab beberapa pertanyaan:
    
        Enter keystore password:  
        Re-enter new password: 
        What is your first and last name?
          [Unknown]:  Aplikasi Authserver
        What is the name of your organizational unit?
          [Unknown]:  Training Microservices
        What is the name of your organization?
          [Unknown]:  ArtiVisi
        What is the name of your City or Locality?
          [Unknown]:  Jakarta
        What is the name of your State or Province?
          [Unknown]:  DKI Jakarta
        What is the two-letter country code for this unit?
          [Unknown]:  ID
        Is CN=Aplikasi Authserver, OU=Training Microservices, O=ArtiVisi, L=Jakarta, ST=DKI Jakarta, C=ID correct?
          [no]:  yes

2. Konfigurasi JWT di aplikasi Spring Boot

    ```java
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.tokenStore(new JdbcTokenStore(dataSource))
                .accessTokenConverter(jwtTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyPair keyPair = new KeyStoreKeyFactory(
                keystoreFile, keystorePassword.toCharArray())
                .getKeyPair(keypairAlias);
        converter.setKeyPair(keyPair);
        return converter;
    }
    ```

3. Sediakan handler untuk mengakses public key di [http://localhost:8080/oauth/token_key](http://localhost:8080/oauth/token_key)

    ```java
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.checkTokenAccess("permitAll()")
        .tokenKeyAccess("permitAll()");
    }
    ```
    
    Hasilnya, public key bisa diakses di url [http://localhost:8080/oauth/token_key](http://localhost:8080/oauth/token_key)
    
    [![JWT Public Key](docs/img/token-key-access.png)](docs/img/token-key-access.png)

4. Lakukan flow seperti di atas, hasilnya bentuk token akan berubah menjadi JWT

    [![Access Token JWT](docs/img/jwt-access-token.png)](docs/img/jwt-access-token.png)

5. Token JWT bisa dicek isinya di website [JWT.io](https://jwt.io)

    [![JWT Decode](docs/img/jwt-decode.png)](docs/img/jwt-decode.png)

6. Kita bisa verifikasi keabsahan token dengan memasang public key untuk verifikasi signature. Public key bisa didapat dari endpoint [http://localhost:8080/oauth/token_key](http://localhost:8080/oauth/token_key)

    [![Verifikasi JWT](docs/img/jwt-verify-signature.png)](docs/img/jwt-verify-signature.png)

